#!/bin/sh

mkdir -p /docker

if [ ! -f /docker/ROOT/manage.py ]
then
	echo '"manage.py" not found, deploying app...'
	cp -rf /setup/ROOT /docker/.
	echo 'deployed app'
fi

if [ ! -f /docker/ROOT/migrated ]
then
	echo 'migration indicator not found, running migration and superuser creation'
	python3 /docker/ROOT/manage.py makemigrations
	python3 /docker/ROOT/manage.py migrate
	echo "from django.contrib.auth.models import User; User.objects.create_superuser(\"$ADMIN_NAME\",\"$ADMIN_EMAIL\",\"$ADMIN_PASSWORD\")" | python3 /docker/ROOT/manage.py shell
	touch /docker/ROOT/migrated
	echo 'migration performed, superuser created, migration indicator set'
fi

echo 'cleaning up...'

rm -rf /setup
export ADMIN_NAME=''
export ADMIN_EMAIL=''
export ADMIN_PASSWORD=''

echo 'starting django...'

python3 /docker/ROOT/manage.py runserver 0.0.0.0:$PORT
