Django==1.8.4
django-admin-bootstrapped==2.5.4
gunicorn
python-gnupg
django-extensions
pytest
pytest-django
pendulum
