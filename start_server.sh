#!/usr/bin/env bash
source env/bin/activate
gunicorn ROOT.wsgi --timeout 3000 --chdir ROOT/ --bind :8000
