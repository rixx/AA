#!/bin/bash -x
PORT=8000

#virtualenv env -p /usr/bin/python3 --always-copy
#source env/bin/activate
sudo pip3 install -r requirements.txt
python3 ROOT/manage.py makemigrations
python3 ROOT/manage.py migrate
python3 ROOT/manage.py createsuperuser
python3 ROOT/manage.py runserver 0.0.0.0:$PORT
