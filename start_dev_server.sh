#!/bin/bash -x
PORT=8000

virtualenv env -p /usr/bin/python3
source env/bin/activate
pip3 install -r requirements.txt
python3 ROOT/manage.py migrate
python3 ROOT/manage.py createsuperuser
python3 ROOT/manage.py runserver 0.0.0.0:$PORT
