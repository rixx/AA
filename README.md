# Administration Association #

This App will help you to administrate associations :P

**WARNING:** WARRANTY VOID ON COMMIT

## How to... ##

### Install on Vagrant ###

Start Vagrant
```bash
cd AA
vagrant up
vagarnt ssh
```

Within the VM
```bash
cd /vagrant
./install_and_start_django_server_vagrant.sh
```
You will be promted for the DB-User and Password

### Run Server ###

```bash
cd AA
python3 ROOT/manage.py runserver 0.0.0.0:8000
```

### Install with Docker ###
Requires: docker, docker-compose

Open docker-compose.yml in your text editor and change the values after ADMIN_*= to something else; they determine the admin login.

To start
```bash
cd AA
docker-compose up -d
```
Ommit -d to start in attached mode allowing you to see console output.

Data is persisted unsing a volume one the docker host at /var/lib/docker/persistent-volumes/pythonfoo/AA/.
On osx and windows the docker daemon can't run natively, therefore it runs in a minimalistic vm, the docker-machine. On those system you need to use
```bash
docker-machine ssh
```
from a docker enabled shell to access the volume (e.g. to clear its contents)

You can also use SSHFS to mount a folder from your host system into the docker-machine, on osx there is a tool called docker-machine-nfs available which can be installed via homebrew. The use of the virtualbox shared folder is possible in theory but has a major performance impact on disk operations.

To shut down
```bash
cd AA
docker-compose down
```

After starting the container for the first time, the admin user will be written to the database which is persistet on the volume. The Environment variables are no longer necessary after that point and can be commented out or be removed from the docker-compose.yml followed by a restart of the container to make them unavailabe within the container.


Notes:
A non file based database should not be installed within the same container but a seperate one defined as a second service in the docker-compose.yml and is reachable from all other containers started from that docker-compose.yml by using the service name as host name as is with all other services exposing Ports in their dockerfile.
To add an nginx reverse-proxy for example add a new container with an nginx, exposing a port mapped to the outside in the docker-compose.yml. Since the nginx will be the the service reachable form the outside, the port mapping for django could be removed from the docker-compose.yml after the nginx is configured to redirect requests to django:8000
