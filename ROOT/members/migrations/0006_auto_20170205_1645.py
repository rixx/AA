# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0005_member_last_update'),
    ]

    operations = [
        migrations.CreateModel(
            name='EmailAddress',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('email_address', models.EmailField(max_length=254)),
                ('is_primary', models.BooleanField(default=False)),
                ('gpg_key_id', models.CharField(max_length=40, blank=True)),
                ('gpg_fingerprint', models.CharField(max_length=40, blank=True)),
                ('gpg_error', models.CharField(default=' ', max_length=1, choices=[('r', 'revoked'), ('e', 'expired'), ('?', 'key not found'), ('i', 'invalid key_id'), ('m', 'multiple keys found'), ('n', 'no keys found'), ('M', 'multiple keys imported'), ('u', 'unknown error'), (' ', 'no error')])),
                ('last_checked', models.DateTimeField(null=True, blank=True)),
            ],
            options={
                'verbose_name_plural': 'email addresses',
                'verbose_name': 'email address',
            },
        ),
        migrations.CreateModel(
            name='EmailToMember',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('subject', models.CharField(max_length=255, blank=True)),
                ('body', models.TextField(blank=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('email_type', models.CharField(default='def', max_length=3, choices=[('def', 'default'), ('dml', 'data record'), ('wlc', 'welcome'), ('dep', 'delayed payment')])),
            ],
            options={
                'verbose_name_plural': 'emails to send',
            },
        ),
        migrations.RemoveField(
            model_name='email',
            name='member',
        ),
        migrations.RemoveField(
            model_name='emailtosend',
            name='member',
        ),
        migrations.AlterField(
            model_name='member',
            name='last_update',
            field=models.DateField(default=datetime.datetime(2017, 2, 5, 16, 45, 6, 257968), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='member',
            name='membership_start',
            field=models.DateField(auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='member',
            name='membership_type',
            field=models.CharField(default='SUP', max_length=3, choices=[('SUP', 'SUPPORTER'), ('MBR', 'MEMBER'), ('HON', 'HONORARY'), ('RMS', 'SUSPENDED')]),
        ),
        migrations.DeleteModel(
            name='Email',
        ),
        migrations.DeleteModel(
            name='EmailToSend',
        ),
        migrations.AddField(
            model_name='emailtomember',
            name='member',
            field=models.ForeignKey(to='members.Member'),
        ),
        migrations.AddField(
            model_name='emailaddress',
            name='member',
            field=models.ForeignKey(to='members.Member'),
        ),
    ]
