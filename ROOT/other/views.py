from django.conf import settings
from django.shortcuts import render


def index(request):
    hasDatenschleuder = 'datenschleuder' in settings.INSTALLED_APPS
    return render(request, "index_other.html", {'hasDatenschleuder': hasDatenschleuder})
